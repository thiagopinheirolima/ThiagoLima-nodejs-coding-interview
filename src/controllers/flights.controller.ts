import {
    JsonController,
    Get,
    Body,
    Param,
    Post,
    OnUndefined,
} from 'routing-controllers'
import { FlightsService } from '../services/flights.service'
import { PersonsService } from '../services/persons.service'

const flightsService = new FlightsService()
const personsService = new PersonsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        const data = await flightsService.getAll()
        return {
            status: 200,
            data,
        }
    }

    @OnUndefined(400)
    @Post('/:id/onboard', { transformResponse: false })
    async onboard(
        @Param('id') id: string,
        @Body() body: { passengerId: string }
    ) {
        const { passengerId } = body
        const flight = await flightsService.getOne(id)
        const passenger = await personsService.getOne(passengerId)

        if (flight && passenger) {
            flight.passengers.push(passengerId)
            await flight.save()
            return {
                status: 201,
                data: flight,
            }
        }
    }
}
