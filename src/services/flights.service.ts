import { FlightsModel } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async getOne(id: string) {
        try {
            return await FlightsModel.findById(id)
        } catch (error) {
            return null
        }
    }
}
